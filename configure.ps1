# Disable Automatic Windows Updates
Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name AUOptions -Value 1

# Enable WDigest to store clear text passwords in LSASS
Set-ItemProperty -Path HKLM:\System\CurrenctControlSet\Control\SecurityProvider\WDigest -Name UseLogonCredential -Value 1

