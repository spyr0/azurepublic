Import-Module ActiveDirectory

New-ADOrganizationalUnit -Name Databases -Path "DC=evilcorp,DC=uk" -Description "EvilCorp Owned Databases"
New-ADOrganizationalUnit -Name Workstations -Path "DC=evilcorp,DC=uk" -Description "Client Workstations"