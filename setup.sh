#!/bin/bash

# Azure NV* Series Hashcat setup - run as root
sudo su

# Handle pre-reqs - Now being done by different artifact
#apt -y install gcc make p7zip-full git
apt update –y
apt upgrade -y

# Download NVIDIA drivers
wget -O /home/wsadmin/NVIDIA-Linux-x86_64.run http://uk.download.nvidia.com/tesla/418.67/NVIDIA-Linux-x86_64-418.67.run
chmod +x /home/wsadmin/NVIDIA-Linux-x86_64.run

# Download Hashcat binaries
wget -O /home/wsadmin/hashcat-5.1.0.7z https://hashcat.net/files/hashcat-5.1.0.7z
7z x /home/wsadmin/hashcat-5.1.0.7z -o/home/wsadmin/

# Housekeeping
echo 'blacklist nouveau' >> /etc/modprobe.d/blacklist.conf
echo 'options nouveau modeset=0' >> /etc/modprobe.d/blacklist.conf

# Silent install of drivers
/home/wsadmin/NVIDIA-Linux-x86_64.run -q -a -n -X -s

# Benchmark to confirm everything working
/home/wsadmin/hashcat-5.1.0/hashcat64.bin -b -m 1000 > /home/wsadmin/benchmark_test.txt

# Cleanup
rm -f /home/wsadmin/hashcat-5.1.0.7z /home/wsadmin/NVIDIA-Linux-x86_64.run

# Password cracking wordlists
git clone https://github.com/NotSoSecure/password_cracking_rules.git /home/wsadmin/ortrta
git clone https://github.com/danielmiessler/SecLists.git /home/wsadmin/SecLists